package CryptoBack.service;

import org.springframework.web.client.RestTemplate;

public class TickerValueService {

  /*  private String currency;

    public TickerValueService(String currency) {
        this.currency = currency;
    }

 /*   public static double getMarketValue(double amount, String symbol) {

        String uri = "https://api-pub.bitfinex.com/v2/ticker/" + symbol;
        RestTemplate restTemplate = new RestTemplate();


        CurrentValue currentPrice = restTemplate.getForObject(uri, CurrentValue.class);
        double currentMarketValue = currentPrice.getMarketValue(amount);
      /*  if (symbol.equals("XRPUSD")){
            double convertedMarketValue = TickerValueService.getMarketValue(currentMarketValue,"tBTCEUR");
            return (Math.round(convertedMarketValue*100)/100.0);
        }
        return (Math.round(currentMarketValue * 100) / 100.0);
    }*/

    public static double getMarketValue(int amount, String currency) {
    String symbol = "";
            switch (currency) {
                case ("Bitcoin"):
                   symbol = "tBTCEUR";
                    break;
                case ("Ethereum"):
                    symbol = "tETHEUR";
                    break;
                case ("Ripple"):
                    symbol = "tXRPUSD";
                    break;
            }
        String uri = "https://api-pub.bitfinex.com/v2/ticker/" + symbol;
        RestTemplate restTemplate = new RestTemplate();
        String[] cryptoTicker = restTemplate.getForObject(uri, String[].class);
        double lastPrice = Math.round(Double.parseDouble(cryptoTicker[6])*100.0)/100.0;
        double marketValue = amount*lastPrice;
        System.out.println(lastPrice);
        System.out.println(marketValue);

        return marketValue;
    }

    public static void main(String[] args) {
        getMarketValue(2,"Ethereum");
    }
}
