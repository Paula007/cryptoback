package CryptoBack.controller;

import CryptoBack.model.Crypto;
import CryptoBack.repository.CryptoRepository;
import CryptoBack.service.TickerValueService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:8082") // see on frondi port
@RestController
@RequestMapping("/api")
public class CryptoController {

    @Autowired
    CryptoRepository cryptoRepository;

    private void getMarketValue(Crypto crypto) {

        double currentMarketValue = TickerValueService.getMarketValue(crypto.getAmount(), crypto.getCurrency());
        crypto.setValue(currentMarketValue);
    }

    @GetMapping("/cryptos")
    public ResponseEntity<List<Crypto>> getAllCryptos() {

        //   try {
        List<Crypto> cryptos = cryptoRepository.findAll();
        if (cryptos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
       /*     for (Crypto crypto : cryptos) {
                double currentMarketValue = TickerValueService.getMarketValue(crypto.getAmount(), crypto.getCurrency());
                crypto.setValue(currentMarketValue);
            }*/
        cryptos.forEach(this::getMarketValue);
        return new ResponseEntity<>(cryptos, HttpStatus.OK);
      /*  } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }*/
    }


    @GetMapping("/cryptos/{id}")
    public ResponseEntity<Crypto> getCryptoById(@PathVariable("id") int id) {
        Optional<Crypto> cryptoData = cryptoRepository.findById(id);

        if (cryptoData.isPresent()) {
            return new ResponseEntity<>(cryptoData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/cryptos")
    public ResponseEntity<Crypto> createCrypto(@RequestBody Crypto crypto) {
        try {
            Crypto _crypto = cryptoRepository.save(new Crypto(crypto.getCurrency(), crypto.getAmount(),
                    crypto.getLocation(), crypto.getDate()));
            return new ResponseEntity<>(_crypto, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/cryptos/{id}")
    public ResponseEntity<Crypto> updateCrypto(@PathVariable("id") int id, @RequestBody Crypto crypto) {
        Optional<Crypto> cryptoData = cryptoRepository.findById(id);

        if (cryptoData.isPresent()) {
            Crypto _crypto = cryptoData.get();
            _crypto.setCurrency(crypto.getCurrency());
            _crypto.setAmount(crypto.getAmount());
            _crypto.setLocation(crypto.getLocation());
            _crypto.setDate(crypto.getDate());
            return new ResponseEntity<>(cryptoRepository.save(_crypto), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/cryptos/{id}")
    public ResponseEntity<HttpStatus> deleteCrypto(@PathVariable("id") int id) {
        try {
            cryptoRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/cryptos")
    public ResponseEntity<HttpStatus> deleteAllCryptos() {
        try {
            cryptoRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
